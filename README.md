# Teva NHS Touchscreen #
## READ ME ##
### What is this repository for? ###

* This is the Teva touchscreen created for the NHS innovation stand for september 2016.
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* To set up the touchscreen, you will need Node.js and gulp installed on your machine. 
* First of all install Node Package Manager from https://nodejs.org/en/download/ 
* Now install gulp https://www.npmjs.com/package/gulp-install
* Go to the root of the cloned repository in your Terminal/CMD and install dependancies by running 'npm install'
* If gulp has not been installed locally, do so by running 'gulp install'
* Run the command 'gulp' and a browser sync tab will open 
* Finally set your chrome/firefox mobile view dimentions to 1080x1920

### Who do I talk to? ###

* Mia Song song@pulsarhealthcare.com
* Tim Walton tim@pulsarhealthcare.com
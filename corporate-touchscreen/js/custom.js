$(document).ready(function() {

	//This takes the current time and applies it to the variable time
	var time = new Date().getTime();
	//Here we set the variable of time to the date of the last click press
	$(document.body).bind("click", function(e) {
	 	time = new Date().getTime();
	});

	// this function is the time out, which refreshes the page after 120 seconds of no interaction
	function refresh() {
	if(new Date().getTime() - time >= 120000) 
		window.location.reload(true);
	else 
		setTimeout(refresh, 10000);
	}

	// when the enter button is clicked (n the home page) the following is triggered
	$('#enter').on('click', function(){
		setTimeout(refresh, 10000); //this is the call for the refresh function, because we dont want to refresh the page when it is on the home page, we wait until this point
	    $('#welcome').hide(); //this hides the welcome page
	    // this method applies the active state/class to the menus
	    $('div','#menu').each(function(){ 
	    	if($(this).attr('id')!=='menu_icon') {
	    		var menuButtonId = $(this).attr('id')+'_active';
	     		$(this).addClass(menuButtonId); 
	     	}
	    });
	  });
	
	//checks for when a specific menu option selected 
	$('#menu div').on('click', function(){
	  	var clickedDiv = $(this).attr('id');
	  	if (clickedDiv=='work_with_nhs_desc') { 
	  		//does nothing when .work_with_nhs_desc is tapped (It isnt meant to be triggerable)
	  	}
	  	else {
			if ($('#menu_icon').attr('class')=='menu_icon menu_icon_active' || $('#teva_logo').attr('class')=='teva_logo teva_logo_active') {
		  		restore_menu(); //sets the menu items to their original 'active' state
			} else {
			  	$('div','#menu').each(function(){
			    	var menuButtonId = $(this).attr('id'); //gets the selected .each()
			    	if(menuButtonId==clickedDiv) { //if it is the same as the the clicked menu section
				     	$(this).removeClass(menuButtonId+'_active'); //remove the state of active	     	
				     	$(this).addClass('menu_selected'); //gives the class to position it left right, the only visible menu
			    	}
			    	else{
				     	$(this).removeClass(menuButtonId+'_active');  //remove the states of active	
			    	}
			    });
			  	if (clickedDiv=='managing') { 
			  		$('#references').hide();
			  	}else{
			  		$('#references').show();
			  	}
				$('#menu_icon').addClass('menu_icon_active'); //make the menu icon pop out when something is chosen
				$('#teva_logo').addClass('teva_logo_active'); //make the teva logo active when something is chosen
				$('#content').addClass('content_active'); //make the content active pop out when something is chosen
				$('#menu_arrows').addClass('menu_arrows_active'); //make the menu arrows active when something is chosen
				//display tabs for menu option selected 
				var tab_html = "<ul id='tab' class='tab'>"; //this will be the innerhtml
				if (clickedDiv=='managing') {
					var tab_no = 0;
					$('#menu_arrows').removeClass('menu_arrows_active'); //hide menu arrows off screen
				}else if(clickedDiv=='harnessing'){
					var tab_no = 7;
				}else if(clickedDiv=='joint'){
					var tab_no = 2;
				}else if(clickedDiv=='central'){
					var tab_no = 3;
				}else if(clickedDiv=='respiratory'){
					var tab_no = 0;
					$('#menu_arrows').removeClass('menu_arrows_active'); //hide menu arrows off screen
				}else if(clickedDiv=='driving'){
					var tab_no = 4;
				}else if(clickedDiv=='teva'){
					var tab_no = 3;
				}
				for (t = 1; t <= tab_no; t++) {
					tab_html += "<li id='"+clickedDiv+"-tab-"+t+"'><img src='images/content/"+clickedDiv+"/"+clickedDiv+"-tab-"+t+".png'></li>";
				}
				tab_html += "</ul>"; 
				$('#tabs').html(tab_html);
				var res = clickedDiv.split("-"); 
				var tab = '#'+res[0]+'-tab-1';  //1st instance use tab = 1
	  			$(tab).addClass('li_active'); //add class (green) to tab
			  	var image = 'images/content/'+res[0]+'/'+res[0]+'-content-1.png';
				$('#content_img').html('<img src='+image+'>');
			  	if(tab_no==0) {
			  		var menu_arrow_html = '';
			  	}else{
				  	var menu_arrow_html =  '<div id="menu_arrows_right" class="menu_arrows_right">';
				  		menu_arrow_html += '<img src=images/menu/menu_arrows_right.png>';
				  		menu_arrow_html += '</div>';
			  	}	
			  	document.getElementById("menu_arrows").innerHTML = menu_arrow_html;
	  			scrollerCall();
				hotspot_test(clickedDiv,1);	
			}
		}
	});

	//when the tab inside the menu option is clicked 
	$('#tabs').on('click', 'li',function(){
		$('.back_to_teva').hide(); //hide back buttons
		$('.back_to_driving').hide(); //hide back buttons
		var clickedTab = $(this).attr('id');
		$('#tab li').removeClass('li_active');
		$(this).addClass('li_active');
		var res = clickedTab.split("-"); 
		var image = 'images/content/'+res[0]+'/'+res[0]+'-content-'+res[2]+'.png';
		document.getElementById("content_img").innerHTML = '<img src='+image+'>';
		scrollerCall();
		if (res[0]=='managing') {
			var tab_no = 0;
		}else if(res[0]=='harnessing'){
			var tab_no = 7;
		}else if(res[0]=='joint'){
			var tab_no = 2;
		}else if(res[0]=='central'){
			var tab_no = 3;
		}else if(res[0]=='respiratory'){
			var tab_no = 0;
		}else if(res[0]=='driving'){
			var tab_no = 4;
		}else if(res[0]=='teva'){
			var tab_no = 3;
		}
		//need to display correct left right arrow options
		if (res[2] < tab_no && res[2] > 1) { //can tab right & left
			var menu_arrow_html =  '<div id="menu_arrows_left" class="menu_arrows_left">';
		  		menu_arrow_html += '<img src=images/menu/menu_arrows_left.png>';
		  		menu_arrow_html += '</div>';
				menu_arrow_html +=  '<div id="menu_arrows_right" class="menu_arrows_right">';
		  		menu_arrow_html += '<img src=images/menu/menu_arrows_right.png>';
		  		menu_arrow_html += '</div>';
			document.getElementById("menu_arrows").innerHTML = menu_arrow_html;
		}else if (res[2] == 1 ) { //can only tab right
			var menu_arrow_html =  '<div id="menu_arrows_right" class="menu_arrows_right">';
		  		menu_arrow_html += '<img src=images/menu/menu_arrows_right.png>';
		  		menu_arrow_html += '</div>';
			document.getElementById("menu_arrows").innerHTML = menu_arrow_html;
		}else if (res[2] == tab_no ) { //can only tab left
			var menu_arrow_html =  '<div id="menu_arrows_left" class="menu_arrows_left">';
		  		menu_arrow_html += '<img src=images/menu/menu_arrows_left.png>';
		  		menu_arrow_html += '</div>';
			document.getElementById("menu_arrows").innerHTML = menu_arrow_html;
		}
		hotspot_test(res[0],res[2]);
	});	

	//when the tab inside the menu option is clicked - left
	$('#menu_arrows').on('click', '#menu_arrows_left',function(){
		$('.back_to_teva').hide(); //hide back buttons
		$('.back_to_driving').hide(); //hide back buttons
		var src = $('#content_img img').attr('src'); //get current image src
	  	var src_res = src.split("."); 
	  	var path_res = src_res[0].split("/"); //menu option
	  	var image_res = path_res[3].split("-");
		if (path_res[2]=='managing') {
			var tab_no = 0;
		}else if(path_res[2]=='harnessing'){
			var tab_no = 7;
		}else if(path_res[2]=='joint'){
			var tab_no = 2;
		}else if(path_res[2]=='central'){
			var tab_no = 3;
		}else if(path_res[2]=='respiratory'){
			var tab_no = 0;
		}else if(path_res[2]=='driving'){
			var tab_no = 4;
		}else if(path_res[2]=='teva'){
			var tab_no = 3;
		}
		if(image_res[2]>1){ //not the first - can scroll left
			image_no = image_res[2]-1; // get image number
		  	var image = 'images/content/'+path_res[2]+'/'+path_res[2]+'-content-'+image_no+'.png'; //construct image and path
		  	document.getElementById("content_img").innerHTML = '<img src='+image+'>'; //insert image tag into html  
		  	$('#tab li').removeClass('li_active');  //reset selected tab to none
			var tab = '#'+path_res[2]+'-tab-'+image_no; //which tab has been select
		  	$(tab).addClass('li_active'); //add class (green) to tab selected
		}
		scrollerCall();
		if (image_no < tab_no && image_no > 1) { //can tab right & left
			var menu_arrow_html =  '<div id="menu_arrows_left" class="menu_arrows_left">';
		  		menu_arrow_html += '<img src=images/menu/menu_arrows_left.png>';
		  		menu_arrow_html += '</div>';
				menu_arrow_html +=  '<div id="menu_arrows_right" class="menu_arrows_right">';
		  		menu_arrow_html += '<img src=images/menu/menu_arrows_right.png>';
		  		menu_arrow_html += '</div>';
			document.getElementById("menu_arrows").innerHTML = menu_arrow_html;
		}else if (image_no == 1 ) { //can only tab right
			var menu_arrow_html =  '<div id="menu_arrows_right" class="menu_arrows_right">';
		  		menu_arrow_html += '<img src=images/menu/menu_arrows_right.png>';
		  		menu_arrow_html += '</div>';
			document.getElementById("menu_arrows").innerHTML = menu_arrow_html;
		}else if (image_no == tab_no ) { //can only tab left
			var menu_arrow_html =  '<div id="menu_arrows_left" class="menu_arrows_left">';
		  		menu_arrow_html += '<img src=images/menu/menu_arrows_left.png>';
		  		menu_arrow_html += '</div>';
			document.getElementById("menu_arrows").innerHTML = menu_arrow_html;
		}
		hotspot_test(path_res[2],image_no);
	});	
	//when the tab inside the menu option is clicked - right
	$('#menu_arrows').on('click', '#menu_arrows_right',function(){	
		$('.back_to_teva').hide(); //hide back buttons
		$('.back_to_driving').hide(); //hide back buttons	
		var src = $('#content_img img').attr('src'); //get current image src
	  	var src_res = src.split("."); 
	  	var path_res = src_res[0].split("/"); //menu option
	  	var image_res = path_res[3].split("-");
		if (path_res[2]=='managing') {
			var tab_no = 0;
		}else if(path_res[2]=='harnessing'){
			var tab_no = 7;
		}else if(path_res[2]=='joint'){
			var tab_no = 2;
		}else if(path_res[2]=='central'){
			var tab_no = 3;
		}else if(path_res[2]=='respiratory'){
			var tab_no = 0;
		}else if(path_res[2]=='driving'){
			var tab_no = 4;
		}else if(path_res[2]=='teva'){
			var tab_no = 3;
		}
		if(image_res[2]<tab_no){ //not the first - can scroll left
			image_no = parseInt(image_res[2])+1; // get image number
		  	var image = 'images/content/'+path_res[2]+'/'+path_res[2]+'-content-'+image_no+'.png'; //construct image and path
		  	document.getElementById("content_img").innerHTML = '<img src='+image+'>'; //insert image tag into html  
		  	$('#tab li').removeClass('li_active'); //reset selected tab to none
			var tab = '#'+path_res[2]+'-tab-'+image_no; //which tab has been select
		  	$(tab).addClass('li_active'); //add class (green) to tab selected
		}
		scrollerCall();
		if (image_no < tab_no && image_no > 1) { //can tab right & left
			var menu_arrow_html =  '<div id="menu_arrows_left" class="menu_arrows_left">';
		  		menu_arrow_html += '<img src=images/menu/menu_arrows_left.png>';
		  		menu_arrow_html += '</div>';
				menu_arrow_html +=  '<div id="menu_arrows_right" class="menu_arrows_right">';
		  		menu_arrow_html += '<img src=images/menu/menu_arrows_right.png>';
		  		menu_arrow_html += '</div>';
			document.getElementById("menu_arrows").innerHTML = menu_arrow_html;
		}else if (image_no == 1 ) { //can only tab right
			var menu_arrow_html =  '<div id="menu_arrows_right" class="menu_arrows_right">';
		  		menu_arrow_html += '<img src=images/menu/menu_arrows_right.png>';
		  		menu_arrow_html += '</div>';
			document.getElementById("menu_arrows").innerHTML = menu_arrow_html;
		}else if (image_no == tab_no ) { //can only tab left
			var menu_arrow_html =  '<div id="menu_arrows_left" class="menu_arrows_left">';
		  		menu_arrow_html += '<img src=images/menu/menu_arrows_left.png>';
		  		menu_arrow_html += '</div>';
			document.getElementById("menu_arrows").innerHTML = menu_arrow_html;
		}
		hotspot_test(path_res[2],image_no);
	});

	//when hotspot buttons clicked - listed below 
	$('.respiratory_button, .joint_button, .harnessing_button, .driving_joint_button, .back_to_driving, .back_to_teva').on('click', function(){ 
		restore_menu();
		var clickedDiv = '';
	  	if($(this).attr('class')=='respiratory_button'){
	  		clickedDiv = 'respiratory';
	  	}else if ($(this).attr('class')=='joint_button') {
	  		clickedDiv = 'joint';
	  	}else if ($(this).attr('class')=='harnessing_button') {
	  		clickedDiv = 'harnessing';
	  	}else if ($(this).attr('class')=='driving_joint_button') {
	  		clickedDiv = 'joint';
	  	}else if ($(this).attr('class')=='back_to_teva') {
	  		clickedDiv = 'teva';
	  	}else if ($(this).attr('class')=='back_to_driving') {
	  		clickedDiv = 'driving';
	  	}
	  	$('div','#menu').each(function(){
	    	var menuButtonId = $(this).attr('id'); //gets the selected .each()
	    	if(menuButtonId==clickedDiv) { //if it is the same as the the clicked menu section
		     	$(this).removeClass(menuButtonId+'_active'); //remove the state of active	     	
		     	$(this).addClass('menu_selected'); //gives the class to position it left right, the only visible menu
	    	}
	    	else{
		     	$(this).removeClass(menuButtonId+'_active');  //remove the states of active	
	    	}
	    });
		$('#menu_icon').addClass('menu_icon_active'); //make the menu icon pop out when something is chosen
		$('#teva_logo').addClass('teva_logo_active'); //make the teva logo active when something is chosen
		$('#content').addClass('content_active'); //make the content active pop out when something is chosen
		$('#menu_arrows').addClass('menu_arrows_active'); //make the menu arrows active when something is chosen
		//display tabs for menu option selected 
		var tab_html = "<ul id='tab' class='tab'>"; //this will be the innerhtml
		if (clickedDiv=='managing') {
			var tab_no = 0;
			$('#menu_arrows').removeClass('menu_arrows_active'); //hide menu arrows off screen
		}else if(clickedDiv=='harnessing'){
			var tab_no = 7;
		}else if(clickedDiv=='joint'){
			var tab_no = 2;
		}else if(clickedDiv=='central'){
			var tab_no = 3;
		}else if(clickedDiv=='respiratory'){
			var tab_no = 0;
			$('#menu_arrows').removeClass('menu_arrows_active'); //hide menu arrows off screen
		}else if(clickedDiv=='driving'){
			var tab_no = 4;
		}else if(clickedDiv=='teva'){
			var tab_no = 3;
		}
		for (t = 1; t <= tab_no; t++) {
			tab_html += "<li id='"+clickedDiv+"-tab-"+t+"'><img src='images/content/"+clickedDiv+"/"+clickedDiv+"-tab-"+t+".png'></li>";
		}
		tab_html += "</ul>"; 
		document.getElementById("tabs").innerHTML = tab_html;
		var res = clickedDiv.split("-"); 
		var tab = '#'+res[0]+'-tab-1';  //1st instance use tab = 1
			$(tab).addClass('li_active'); //add class (green) to tab
		if ($(this).attr('class')=='respiratory_button'||$(this).attr('class')=='joint_button') {
			$('.back_to_teva').show(); //show back button to return to former menu
		}else if( $(this).attr('class')=='harnessing_button'||$(this).attr('class')=='driving_joint_button') {
			$('.back_to_driving').show(); //show back button to return to former menu
		}else{
			$('.back_to_teva').hide(); //hide back buttons
			$('.back_to_driving').hide(); //hide back buttons
		}
	  	var image = 'images/content/'+res[0]+'/'+res[0]+'-content-1.png';
	  	document.getElementById("content_img").innerHTML = '<img src='+image+'>';
	  	if(tab_no==0) {
	  		var menu_arrow_html = '';
	  	}else{
		  	var menu_arrow_html =  '<div id="menu_arrows_right" class="menu_arrows_right">';
		  		menu_arrow_html += '<img src=images/menu/menu_arrows_right.png>';
		  		menu_arrow_html += '</div>';
	  	}	
	  	document.getElementById("menu_arrows").innerHTML = menu_arrow_html;
		scrollerCall();
		hotspot_test(clickedDiv,1);
	});

	//on click of menu button
	$('#menu_icon').on('click', function(){
		restore_menu();
	});

	//on click if teva logo is active
	$('#teva_logo').on('click', function(){
		restore_menu();
	});


	//click on city with info
	$('.ashford, .bristol, .wirral').on('click', function(){ 
		$('#ongoing_project_wrapper').hide();//hide ongoing project
		if ($('.back_to_teva').css('display')=='block') { //if Back to Teva shown
    		$('#back_to_map').addClass('teva');	//add a class to the Back to map button
			$('.back_to_teva').hide(); //hide teva button
		}
		if ($('.back_to_driving').css('display')=='block') { //if Back to Driving shown
			$('#back_to_map').addClass('driving');	//add a class to the Back to map button
			$('.back_to_driving').hide(); //hide driving button
		}
		$('#back_to_map').show();
		city_img='<img src="images/content/joint/joint-'+$(this).attr('class')+'-content-1.png">';
		document.getElementById("content_img").innerHTML = city_img;
		scrollerCall();   
    }); 
    //return to map 
	$('#back_to_map').on('click', function(){ 
		if ( $('#back_to_map').hasClass( 'driving' ) ) {
			$('#back_to_map').removeClass('driving');//remove class driving
			$('.back_to_driving').show(); //show back to driving
		}
		if ($('#back_to_map').hasClass( 'teva' ) ) { //if class teva is present on  #back_to_map 
			$('#back_to_map').removeClass('teva'); //remove class teva
			$('.back_to_teva').show(); //show back to teva
		}
		$('#back_to_map').hide();
		city_img='<img src="images/content/joint/joint-content-1.png">';
		document.getElementById("content_img").innerHTML = city_img;
		scrollerCall();   
	});	

	//click on city without info - ongoing project
	$('.stockport, .newark, .gloust, .devon').on('click', function(){
		$('#ongoing_project_wrapper').show(); //show ongoing project
		if ($(this).attr('class')=='stockport'){
			$('#ongoing_project_wrapper').css('left','51%');	
			$('#ongoing_project_wrapper').css('top','69%');				
		}else if ($(this).attr('class')=='newark') {
			$('#ongoing_project_wrapper').css('left','61%');	
			$('#ongoing_project_wrapper').css('top','71%');	
		}else if ($(this).attr('class')=='gloust') {
			$('#ongoing_project_wrapper').css('left','51%');	
			$('#ongoing_project_wrapper').css('top','75%');	
		}else if ($(this).attr('class')=='devon') {
			$('#ongoing_project_wrapper').css('left','41%');	
			$('#ongoing_project_wrapper').css('top','84%');	
		}
    }); 
	$('#close_city').on('click', function(){ 
		$('#ongoing_project_wrapper').hide();//hide ongoing project
	});	

	$('#references').on('click', function(){ 
		var src = $('#content_img img').attr('src'); //get current image src
	  	var src_res = src.split("."); 
	  	var path_res = src_res[0].split("/"); //menu option
	  	var ref_img = "";
		if (path_res[2]=='managing') {
			ref_img+='<img src="images/content/managing/managing-reference.png">';
		}else if(path_res[2]=='harnessing'){
			ref_img+='<img src="images/content/harnessing/harnessing-reference.png">';
		}else if(path_res[2]=='joint'){
			ref_img+='<img src="images/content/joint/joint-reference.png">';
		}else if(path_res[2]=='central'){
			ref_img+='<img src="images/content/central/central-reference.png">';
		}else if(path_res[2]=='respiratory'){
			ref_img+='<img src="images/content/respiratory/respiratory-reference.png">';
		}else if(path_res[2]=='driving'){
			ref_img+='<img src="images/content/driving/driving-reference.png">';
		}else if(path_res[2]=='teva'){
			ref_img+='<img src="images/content/teva/teva-reference.png">';
		}
		$('.modal-content').html(ref_img);
	    $('#myModal').show();
		// When the user clicks on <span> (x), close the modal
		$('#close').on('click', function(){ 
		    $('#myModal').hide();
		});
		// When the user clicks anywhere outside of the modal, close it
		$(window).click(function(event) {
		    if (event.target == document.getElementById('myModal')) {
		        $('#myModal').hide();
		    }
		});
	});
});

function restore_menu() {
  	$('.menu_selected').removeClass('menu_selected');
    $('div','#menu').each(function(){
    	if($(this).attr('id')!=='menu_icon') {
    		var menuButtonId = $(this).attr('id')+'_active';
     		$(this).addClass(menuButtonId);
     	}
    });
    $('#tabs').empty();//make sure tabs are clear
    $('#content_img').empty();//make sure content is clear
    $('#menu_arrows').empty();//make sure menu arrows are clear
	$('#menu_icon').removeClass('menu_icon_active'); //hide menu icon off screen 
	$('#teva_logo').removeClass('teva_logo_active'); //make the teva logo inactive
	$('#content').removeClass('content_active'); //hide content off screen
	$('#menu_arrows').removeClass('menu_arrows_active'); //hide menu arrows off screen
	$('.cities').hide(); //hide teva buttons
	$('#teva_buttons').hide(); //hide teva buttons
	$('#driving_buttons').hide(); //hide teva buttons
	$('.back_to_teva').hide(); //hide back buttons
	$('.back_to_driving').hide(); //hide back buttons
	$('#cities_content').hide();
	$('#ongoing_project_wrapper').hide();//hide ongoing project
}

function scrollerCall() {
	// $(".scroller").mCustomScrollbar("disable",true);
	// $(".scroller").mCustomScrollbar("update");
	// $.mCustomScrollbar.defaults.axis="y";
 //    $(".scroller").mCustomScrollbar({theme:"3d-thick"});
 //    $(".all-themes-switch a").click(function(e){
 //        e.preventDefault();
 //         var $this=$(this),
 //             rel=$this.attr("rel"),
 //             el=$(".content");
 //         switch(rel){
 //             case "toggle-content":
 //                 el.toggleClass("expanded-content");
 //                 break;
 //         }
 //    }); 
}

function hotspot_test (menu,tab){
	if (menu == 'teva' && tab == 1) { //teva selected then tab 1 by default so show button hotspots
		$('#teva_buttons').show();
	}else{
		$('#teva_buttons').hide();
	}
	if (menu == 'joint' && tab == 1) { //joint selected then tab 1 by default so show button hotspots
		$('.cities').show();
	}else{
		$('.cities').hide();
		$('#ongoing_project_wrapper').hide();//hide ongoing project
	}
	if (menu == 'driving' && tab == 1) { //sustainability selected then tab 1 by default so show button hotspots
		$('#driving_buttons').show();
	}else{
		$('#driving_buttons').hide();
	}

}